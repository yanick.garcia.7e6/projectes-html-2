<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <xsl:apply-templates />
</xsl:template>

  <xsl:template match="videojuegos/videojuego">
	  <article class="juego">
      <div>
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="infoGeneral/foto"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="nombre"></xsl:value-of>
            </xsl:attribute>
        </img>
      </div>
        <h4><xsl:value-of select="nombre"></xsl:value-of></h4>
          <p class="descripcion">
              <xsl:value-of select="infoGeneral/descripcion"></xsl:value-of>
          </p>
          <div class="button">
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="infoGeneral/enlace"></xsl:value-of>
              </xsl:attribute>
              Leer más
            </a>
          </div>
    </article>
  </xsl:template>
</xsl:stylesheet>