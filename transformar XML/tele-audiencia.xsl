<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="programacio/audiencia">
        <cadena>
            <programas>
                <nom><xsl:value-of select="cadenes/cadena[@nom='Un TV']/@nom"></xsl:value-of></nom>
                
                <programa>
                    <xsl:attribute name="hora">
                        <xsl:value-of select="hora" />
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </programa>
            </programas>
        </cadena>
    </xsl:template>

    <xsl:template match="cadenes/cadena[@nom='Un TV']">
        <nom-programa></nom-programa>
        <audiencia></audiencia>
        <xsl:for-each select="cadena[@nom='Un TV']">
            <nom-programa><xsl:value-of select="cadena[@nom='Un TV']"></xsl:value-of></nom-programa>
            <audiencia><xsl:value-of select="cadena[@nom='Un TV']/@percentatge"></xsl:value-of></audiencia>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>