<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <body>
                <h1>M04-Notas</h1>
                <table style="border: 1px solid black">
                <tr style="background-color: green">
                    <th style="text-align:left; border: 1px solid black">nombre</th>
                    <th style="text-align:left; border: 1px solid black">apellidos</th>
                    <th style="text-align:left; border: 1px solid black">telefono</th>
                    <th style="text-align:left; border: 1px solid black">repetidor</th>
                    <th style="text-align:left; border: 1px solid black">nota practica</th>
                    <th style="text-align:left; border: 1px solid black">nota examen</th>
                    <th style="text-align:left; border: 1px solid black">nota final</th>
                    <th style="text-align:left; border: 1px solid black">foto</th>
                </tr>
                <xsl:for-each select="evaluacion/alumno">
                <tr>
                    <th style="border: 1px solid black"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="apellidos"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="telefono"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="@repite"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="notas/practicas"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="notas/examen"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="((notas/practicas + notas/examen) div 2)"></xsl:value-of></th>
                </tr>
                </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
