<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <h1>M04 - Notas</h1>
                <xsl:apply-templates select="evaluacion"/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="evaluacion">
        <table style="border:1px solid black">
            <tr>
                <th style="border:1px solid black; background-color:lime">Nombre</th>
                <th style="border:1px solid black; background-color:lime">Apellidos</th>
                <th style="border:1px solid black; background-color:lime">Telefono</th>
                <th style="border:1px solid black; background-color:lime">Repetidor</th>
                <th style="border:1px solid black; background-color:lime">Nota Practica</th>
                <th style="border:1px solid black; background-color:lime">Nota Examen</th>
                <th style="border:1px solid black; background-color:lime">Nota Total</th>
                <th style="border:1px solid black; background-color:lime">Foto</th>
            </tr>
            <xsl:apply-templates select="alumno"/>
        </table>
    </xsl:template>

    
    <xsl:template match="alumno">
        <tr>
            <td style="border:1px solid black">
                <xsl:value-of select="nombre"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="apellidos"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="telefono"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="@repite"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="notas/practicas"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="notas/examen"/>
            </td>
            <td style="border:1px solid black">
                <xsl:value-of select="((notas/examen + notas/practicas) div 2)"/>
            </td>
            <td style="border:1px solid black">
                <img>
                    <xsl:choose>
                        <xsl:when test="foto">
                            <xsl:attribute name="src">
                                imagenes/alumnos/<xsl:value-of select="foto"/>
                            </xsl:attribute>
                        </xsl:when>

                        <xsl:when test="@foto">
                            <xsl:attribute name="src">
                                imagenes/alumnos/<xsl:value-of select="@foto"/>
                            </xsl:attribute>
                        </xsl:when>

                        <xsl:otherwise>
                            <xsl:attribute name="src">imagenes/Default.png</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:attribute name="alt">
                        <xsl:value-of select="nombre"/>
                    </xsl:attribute>
                    <xsl:attribute name="height">100px</xsl:attribute>
                    <xsl:attribute name="width">100px</xsl:attribute>
                </img>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
