<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="mundo">
        <html>
            <body>
                <xsl:for-each select="continente">
                <h1><xsl:value-of select="nombre"></xsl:value-of></h1>
                <xsl:apply-templates/>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="paises">
                <table style="border: 1px solid green">
                    <tr>
                        <th style="text-align:center; border: 1px solid green">Bandera</th>
                        <th style="text-align:center; border: 1px solid green">País</th>
                        <th style="text-align:center; border: 1px solid green">Gobierno</th>
                        <th style="text-align:center; border: 1px solid green">Capital</th>
                    </tr>
                    <xsl:for-each select="pais">
                    <tr>
                        <th style="border: 1px solid green"><xsl:value-of select="foto"></xsl:value-of></th>
                        <th style="border: 1px solid green"><xsl:value-of select="nombre"></xsl:value-of></th>
                        <th style="border: 1px solid green"><xsl:value-of select="@gobierno"></xsl:value-of></th>
                        <th style="border: 1px solid green"><xsl:value-of select="capital"></xsl:value-of></th>
                    </tr>
                    </xsl:for-each>
                </table>
    </xsl:template>
</xsl:stylesheet>