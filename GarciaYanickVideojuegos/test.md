# Parrafo y salto de linea
para hacer
dos lineas
o utilizar  
doble espaciado al final 

doble salto

# H1
## H2
### H3

formas alternativas

Título 1
==
Título 2
==

**negrita** __negrita__

*cursiva* _cursiva_

***negrita y cursiva*** ___negrita y cursiva___

### codigo
~~~
print:("hola");
~~~

~~~ kt
print:("hola");
~~~

```html
<html>
    <head></head>
    <body>
        <p>Oli<p>
    <body>
</html>
```

### Enlaces

[Enlace] (http://google.com)

[Enlace a Blog](blog) [blog] (http://blogspot.com)
